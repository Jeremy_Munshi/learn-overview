from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=30, blank=True)
    description = models.TextField(verbose_name="About", max_length=500, blank=True)
    price = models.DecimalField(null=False, blank=False, verbose_name="Price", max_digits=19, decimal_places=2)
    duty_percentage = models.DecimalField(null=False, blank=False, verbose_name="Duty Percentage", max_digits=6, decimal_places=2)
    image = models.ImageField(upload_to='%Y/%m/%d/uploads/', blank=False, null=False)

    def __str__(self):
        return '{0}'.format(self.name)