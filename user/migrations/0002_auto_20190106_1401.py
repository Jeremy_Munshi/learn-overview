# Generated by Django 2.1.5 on 2019-01-06 08:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='productquantity',
            options={'verbose_name_plural': 'Product Quantities'},
        ),
    ]
