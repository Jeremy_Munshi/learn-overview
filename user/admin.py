from django.contrib import admin
from .models import Profile
from .models import ProductQuantity

class ProductQuantityInline(admin.TabularInline):
    model = ProductQuantity
    extra = 1

class ProfileAdmin(admin.ModelAdmin):
    inlines = (ProductQuantityInline,)

admin.site.register(Profile, ProfileAdmin)