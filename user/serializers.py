from rest_framework import serializers
from .models import Profile, ProductQuantity
from product.serializers import ProductSerializer
from decimal import Decimal

class ProductQuantitySerializer(serializers.ModelSerializer):
    product = ProductSerializer()
    class Meta:
        model = ProductQuantity
        fields = ('id', 'product', 'amount',)

class ProfileSerializer(serializers.ModelSerializer):
    cart = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    duty_total = serializers.SerializerMethodField()

    def get_duty_total(self, model):
        data = model.cart.through.objects.filter(profile=model)
        i = data.count() - 1
        total = 0

        while i > -1:
            total += data[i].product.price * data[i].product.duty_percentage * data[i].amount * Decimal(0.01)
            i -= 1
        
        return total

    def get_total(self, model):
        data = model.cart.through.objects.filter(profile=model)
        i = data.count() - 1
        total = 0

        while i > -1:
            total += data[i].product.price * data[i].amount
            i -= 1

        #return data[1].product.price + data[0].product.price
        return total

    def get_cart(self, model):
        data = model.cart.through.objects.filter(profile=model)
        return ProductQuantitySerializer(data, many=True).data

    class Meta:
        model = Profile
        fields = ('id', 'url', 'user', 'about', 'birth_date', 'cart', 'total', 'duty_total',)