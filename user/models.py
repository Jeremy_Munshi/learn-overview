from django.db import models
from django.contrib.auth.models import User
from product.models import Product
from django.core.validators import MinValueValidator

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    about = models.TextField(verbose_name="About", max_length=500, blank=True)
    birth_date = models.DateField(
        verbose_name="Birth Date", null=True, blank=True)
    cart = models.ManyToManyField(Product, related_name='cart', through='ProductQuantity',)

    def __str__(self):
        return '{0}'.format(self.user.username)

class ProductQuantity(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(validators=[MinValueValidator(1)])

    class Meta:
        verbose_name_plural = 'Product Quantities'